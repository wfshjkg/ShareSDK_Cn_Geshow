package com.caesar.gxmz.adapter;

import android.content.Context;

import com.caesar.gxmz.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;
import java.util.Map;

/**
 * 文 件 名: AnimationAdapter
 * 创 建 人: Allen
 * 创建日期: 16/12/24 15:33
 * 邮   箱: AllenCoder@126.com
 * 修改时间：16/12/24 15:33
 * 修改备注：
 */
public class AnimationBottomAdapter extends BaseQuickAdapter<Map<String,Object>, BaseViewHolder> {
    private Context context;

    public AnimationBottomAdapter(Context context, List data) {
        super( R.layout.fragment_thinkcmf_recyclerview_bottom_item, data );
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Map<String,Object> map) {
        helper.setImageResource(R.id.animation2_image,(int)map.get("image"));
        helper.setText(R.id.animation2_title,(String)map.get("title"));
    }

}
