package com.caesar.gxmz.easypop;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.caesar.gxmz.R;
import com.caesar.gxmz.adapter.AnimationAdapter;
import com.caesar.gxmz.adapter.AnimationBottomAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.orhanobut.dialogplus.DialogPlus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zyyoona7 on 2018/3/12.
 * <p>
 * PopupWindow 中存在 EditText 隐藏键盘方法不起作用，只有 toggle 键盘方法才起作用
 * 注：建议由 EditText 需求的弹窗使用 DialogFragment
 */
public class CustomerPopUpView extends BasePopup<CustomerPopUpView> {

    private static Context mContext;
    public RecyclerView mRecyclerView1;
    public RecyclerView mRecyclerView2;
    private AnimationAdapter animationAdapter;
    private AnimationBottomAdapter bottomAdapter;
    private BaseQuickAdapter.OnItemClickListener itemClickListener;
    private List<HashMap<String, Object>> popList = new ArrayList<HashMap<String, Object>>();
    private List<HashMap<String, Object>> bottomList = new ArrayList<HashMap<String, Object>>();
    private int[] icons = new int[]{R.drawable.ssdk_oks_classic_wechatmoments, R.drawable.ssdk_oks_classic_wechat, R.drawable.ssdk_oks_classic_wechatfavorite, R.drawable.ssdk_oks_classic_people};
    private String[] titles = new String[]{"朋友圈", "微信好友/群", "一键下载", "人人相册"};
    private int[] iconarray = new int[]{R.drawable.ssdk_oks_classic_tiktok, R.drawable.ssdk_oks_classic_quickhand, R.drawable.ssdk_oks_classic_redbook, R.drawable.ssdk_oks_classic_weibo, R.drawable.ssdk_oks_classic_wetalbum};
    private String[] titlearray = new String[]{"抖音", "快手", "小红书", "微博", "微商相册"};
    public static CustomerPopUpView create(Context context) {
        mContext = context;
        return new CustomerPopUpView(context);
    }

    public CustomerPopUpView(Context context) {
        setContext(context);
    }

    @Override
    protected void initAttributes() {
        setContentView(R.layout.layout_popup_customer, ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(300));
        setFocusAndOutsideEnable(true)
                .setBackgroundDimEnable(true)
                .setAnimationStyle(R.style.BottomPopAnim)
                .setDimValue(0.5f)
                .setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED)
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    protected void initViews(View view, CustomerPopUpView popup) {
        mRecyclerView1 = findViewById(R.id.customer_recyclerview_01);
        mRecyclerView2 = findViewById(R.id.customer_recyclerview_02);
        for (int i = 0; i < icons.length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("image", icons[i]);
            map.put("title", titles[i]);
            popList.add(map);
        }
        for (int i = 0; i < iconarray.length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("image", iconarray[i]);
            map.put("title", titlearray[i]);
            bottomList.add(map);
        }
        animationAdapter = new AnimationAdapter(mContext, popList);
        bottomAdapter = new AnimationBottomAdapter(mContext, bottomList);
        mRecyclerView1.setLayoutManager(new GridLayoutManager(mContext, 2));
        //调整RecyclerView的排列方向
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(mContext, LinearLayout.HORIZONTAL, false));
        animationAdapter.setOnItemClickListener(itemClickListener);
        mRecyclerView1.setAdapter(animationAdapter);
        bottomAdapter.setOnItemClickListener(itemClickListener);
        mRecyclerView2.setAdapter(bottomAdapter);
    }

    public CustomerPopUpView setOnOkClickListener(BaseQuickAdapter.OnItemClickListener listener) {
        itemClickListener = listener;
        return this;
    }
}


