package com.caesar.gxmz;

import cn.jpush.android.api.JPushInterface;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;

public class MyBrocastReceiver extends BroadcastReceiver {

	private String TAG = "Receive";
	private String title;
	private String content;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
    	Bundle bundle = intent.getExtras();
        Log.d(TAG, "onReceive - " + intent.getAction());
         
        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
             
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            System.out.println("收到了自定义消息。消息内容是：" + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            // 自定义消息不会展示在通知栏，完全要开发者写代码去处理
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            System.out.println("收到了通知");
            // 在这里可以做些统计，或者做些其他工作
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            System.out.println("用户点击打开了通知");
            // 在这里可以自己写代码去定义用户点击后的行为
            
           
             title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
             content = bundle.getString(JPushInterface.EXTRA_ALERT);
            System.out.println("title="+title);
            System.out.println("content="+content);


            Intent i = new Intent();
            i.putExtra("title", title);
            i.putExtra("content", content);
            i.setClass(context, ReceiveActivity.class); //自定义打开的界面
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
   
        } else {
            Log.d(TAG, "Unhandled intent - " + intent.getAction());
        }

    }

}
