package com.caesar.gxmz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class TwoActivity extends Activity {

	private WebView webView;
	private SwipeRefreshLayout swipeLayout;

	private static final String TAG = "TWO";

	private ImageView btnBack;
	
	private String urlString = "https://www.fdcanet.com/index.php?app=find_password&appid=1&mobile=1";

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		Log.i(TAG, "onCreate");
		webView = (WebView) this.findViewById(R.id.myWebView_2);
		//
		WebSettings wSet = webView.getSettings();
		wSet.setJavaScriptEnabled(true);
		wSet.setJavaScriptCanOpenWindowsAutomatically(true);
		wSet.setAllowFileAccess(true);// 设置允许访问文件数据
		wSet.setSupportZoom(true);
		wSet.setBuiltInZoomControls(false);
		wSet.setJavaScriptCanOpenWindowsAutomatically(true);
		wSet.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		wSet.setDomStorageEnabled(true);
		wSet.setDatabaseEnabled(true);

		wSet.setCacheMode(WebSettings.LOAD_DEFAULT);
		btnBack = (ImageView) this.findViewById(R.id.btnBack2);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) { // 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
		});

		webView.loadUrl(urlString);
		swipeLayout = (SwipeRefreshLayout) this.findViewById(
				R.id.swipe_container_2);
		swipeLayout
				.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

					@Override
					public void onRefresh() {
						// 重新刷新页面
						webView.loadUrl(webView.getUrl());
					}
				});
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.i(TAG, "onStop");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i(TAG, "onDestroy");
	}

}
