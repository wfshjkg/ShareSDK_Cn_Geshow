package com.caesar.gxmz;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReceiveActivity extends Activity implements OnClickListener {

	private TextView tv_title;
	private TextView tv_content;
	private Button btn_back;
	private LinearLayout ll_back;
	private String title = "";
	private String content = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.receive_activity);
		initData();
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		tv_title = (TextView) this.findViewById(R.id.receive_tv_title);
		tv_title.setText(title);
		tv_content = (TextView) this.findViewById(R.id.receive_tv_content);
		tv_content.setText(content);
		ll_back = (LinearLayout) this.findViewById(R.id.receive_activity_back_ll);
		ll_back.setOnClickListener(this);
	}

	private void initData() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		title = intent.getStringExtra("title");
		content = intent.getStringExtra("content");
		if(("".equals(title) && "".equals(content))){
			Resources res = getResources();
			title = res.getString(R.string.app_name);
			content = "ľ����Ϣ��...������ȥ�ˣ��� -��-|||";
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.receive_activity_back_ll:
			Intent i = new Intent();
			i.setClass(ReceiveActivity.this, MainTabActivity.class);
			startActivity(i);
			finish();
			break;

		}
	}
}
