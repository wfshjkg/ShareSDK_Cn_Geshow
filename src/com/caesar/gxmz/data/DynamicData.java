package com.caesar.gxmz.data;

import java.util.List;

public class DynamicData extends BaseData {

    private DetailData data;

    public DetailData getData() {
        return data;
    }

    public void setData(DetailData data) {
        this.data = data;
    }

    public class DetailData {

        private String user_name;
        private String user_head_img_path;
        private String content;
        private String gallary;
        private List<String> gallary_array;

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_head_img_path() {
            return user_head_img_path;
        }

        public void setUser_head_img_path(String user_head_img_path) {
            this.user_head_img_path = user_head_img_path;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getGallary() {
            return gallary;
        }

        public void setGallary(String gallary) {
            this.gallary = gallary;
        }

        public List<String> getGallary_array() {
            return gallary_array;
        }

        public void setGallary_array(List<String> gallary_array) {
            this.gallary_array = gallary_array;
        }
    }
}
