package com.caesar.gxmz;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.View;

/**
 * 将多张图片拼接在一起
 * 
 * @author yw-tony
 * 
 */
public class JointBitmapView {
	private Bitmap bitmap;

	public JointBitmapView(ArrayList<File> list) {
		bitmap = newBitmap(list);
	}

	/**
	 * 拼接图片
	 * 
	 * @param bit1
	 * @param bit2
	 * @return 返回拼接后的Bitmap
	 */
	private Bitmap newBitmap(ArrayList<File> fileList) {
		Bitmap canMap = null;
		if (fileList.size() < 4) {
			canMap = Bitmap.createBitmap(300, 300 * fileList.size(),
					Bitmap.Config.ARGB_8888);
			for(int i=0;i<fileList.size();i++){
				Bitmap bitMap = BitmapFactory.decodeFile(fileList.get(i).getAbsolutePath());
				Canvas canvas = new Canvas(canMap);
				canvas.drawBitmap(bitMap, i*300, 0, null);	
			}
		} else if (fileList.size() == 4) {
			canMap = Bitmap.createBitmap(600, 600,
					Bitmap.Config.ARGB_8888);
			for(int i=0;i<fileList.size();i++){
				Bitmap bitMap = BitmapFactory.decodeFile(fileList.get(i).getAbsolutePath());
				Canvas canvas = new Canvas(canMap);
				canvas.drawBitmap(bitMap, i%2*300, i/2*300, null);	
			}
		} else if (fileList.size() > 4 && fileList.size() < 7) {
			canMap = Bitmap.createBitmap(600, 900,
					Bitmap.Config.ARGB_8888);
			for(int i=0;i<fileList.size();i++){
				Bitmap bitMap = BitmapFactory.decodeFile(fileList.get(i).getAbsolutePath());
				Canvas canvas = new Canvas(canMap);
				canvas.drawBitmap(bitMap, i%3*300, i/3*300, null);	
			}
		}else if (fileList.size() >= 7 ) {
			canMap = Bitmap.createBitmap(900, 900,
					Bitmap.Config.ARGB_8888);
			for(int i=0;i<fileList.size();i++){
				Bitmap bitMap = BitmapFactory.decodeFile(fileList.get(i).getAbsolutePath());
				Canvas canvas = new Canvas(canMap);
				canvas.drawBitmap(bitMap, i%3*300, i/3*300, null);	
			}
		}
		// 将bitmap放置到绘制区域,并将要拼接的图片绘制到指定内存区域
		return canMap;
	}

}