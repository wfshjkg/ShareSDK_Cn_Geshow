package com.caesar.gxmz;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mathum on 2017-09-26.
 */
public class UserInfoUtils {
    private static void initUserSp(Context context) {
        if (userSp == null) {
            userSp = context.getSharedPreferences(FileName, Context.MODE_PRIVATE);
        }
    }
    private static SharedPreferences userSp;

    public static final String FileName = "UserInfo";
    public static final String KeyAuthToken = "auth_token";
    public static final String KeyPhone = "phone";
    public static final String UserAddress = "address";
    public static final String PayPassWord = "paypassword";
    public static final String UserName = "username";
    public static final String NickName = "nickname";
    public static final String UserSum = "usersum";
    public static final String UserIndustry = "UserIndustry";
    public static final String KeyFirstLogin = "firstLogin";
    public static final String KeyLoginState = "KeyLoginState";
    public static final String AppUserId = "AppUserId";    //id
    public static final String AppUserUrl = "AppUserUrl";   //用户头像
    public static final String UserFollowIDS = "UserFollowID";   //用户关注组
    public static final String UserFollowUIDS = "UserFollowUID";   //用户关注钢厂组
    public static final String UserFriendNum = "UserFriendNum";
    public static final String UserFocusNum = "UserFocusNum";
    public static final String UserPostNum = "UserPostNum";
    public static final String UserType = "UserType";
    public static final String UserFocusPostNum = "UserFocusPostNum";    //用户关注帖子数量
    public static final String PersonalIdentifyState = "PersonalIdentifyState"; //个人实名
    public static final String CompanyIdentifyState = "CompanyIdentifyState";  //公司实名认证
    public static final String UserApplyNum = "UserApplyNum";   //好友申请数量
    public static final String TeamInvitationNum = "TeamInvitationNum";  //团队邀请数量
    public static final String AppMemberState = "AppMemberState";   //app会员
    public static final String APPBalance = "APPBalance";   //app余额
    public static final String APPNeed = "APPNeed";   //首次发布需求
    public static final String APPTeamTask = "APPTeamTask";   //首次发布任务

    public static final String AppImage = "AppImage";   //app会员
    public static final String AppURL = "AppURL";   //app会员

    public static final String UserInfoData1 = "UserInfoData1";
    public static final String UserInfoData2 = "UserInfoData2";
    public static final String UserInfoData3 = "UserInfoData3";
    public static final String UserInfoData4 = "UserInfoData4";
    public static final String UserInfoData5 = "UserInfoData5";
    public static final String UserInfoData6 = "UserInfoData6";

    //首次发布任务
    public static String getAppURL(Context context) {
        initUserSp(context);
        String appURL = userSp.getString(AppURL, "");
        return appURL;
    }

    public static void setAppURL(String appURL, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(AppURL, appURL);
        editor.commit();
    }

    //首次发布任务
    public static String getAppImage(Context context) {
        initUserSp(context);
        String appImage = userSp.getString(AppImage, "");
        return appImage;
    }

    public static void setAppImage(String appImage, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(AppImage, appImage);
        editor.commit();
    }

    //首次发布任务
    public static boolean getAPPTeamTask(Context context) {
        initUserSp(context);
        boolean appTeamTask = userSp.getBoolean(APPTeamTask, true);
        return appTeamTask;
    }

    public static void setAPPTeamTask(boolean appTeamTask, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putBoolean(APPTeamTask, appTeamTask);
        editor.commit();
    }

    //首次发布需求
    public static boolean getAPPNeed(Context context) {
        initUserSp(context);
        boolean appNeed = userSp.getBoolean(APPNeed, true);
        return appNeed;
    }

    public static void setAPPNeed(boolean appNeed, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putBoolean(APPNeed, appNeed);
        editor.commit();
    }

    //app余额
    public static String getAPPBalance(Context context) {
        initUserSp(context);
        String appBalance = userSp.getString(APPBalance, "0");
        return appBalance;
    }

    public static void setAPPBalance(String appBalance, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(APPBalance, appBalance);
        editor.commit();
    }

    //是否会员
    public static int getAppMemberState(Context context) {
        initUserSp(context);
        int appMemberState = userSp.getInt(AppMemberState, 1);
        return appMemberState;
    }

    public static void setAppMemberState(int appMemberState, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(AppMemberState, appMemberState);
        editor.commit();
    }

    //群组邀请数量
    public static int getTeamInvitationNum(Context context) {
        initUserSp(context);
        int teamInvitationNum = userSp.getInt(TeamInvitationNum, 0);
        return teamInvitationNum;
    }

    public static void setTeamInvitationNum(int teamInvitationNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(TeamInvitationNum, teamInvitationNum);
        editor.commit();
    }

    //好友申请数量
    public static int getUserApplyNum(Context context) {
        initUserSp(context);
        int userApplyNum = userSp.getInt(UserApplyNum, 0);
        return userApplyNum;
    }

    public static void setUserApplyNum(int userApplyNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(UserApplyNum, userApplyNum);
        editor.commit();
    }

    //企业认证状态
    public static String getCompanyIdentifyState(Context context) {
        initUserSp(context);
        String companyIdentifyState = userSp.getString(CompanyIdentifyState, "2");
        return companyIdentifyState;
    }

    public static void setPCompanyIdentifyState(String companyIdentifyState, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(CompanyIdentifyState, companyIdentifyState);
        editor.commit();
    }

    //个人认证状态
    public static String getPersonalIdentify(Context context) {
        initUserSp(context);
        String personalIdentifyState = userSp.getString(PersonalIdentifyState, "2");
        return personalIdentifyState;
    }

    public static void setPersonalIdentifyState(String personalIdentifyState, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(PersonalIdentifyState, personalIdentifyState);
        editor.commit();
    }

    //行业
    public static String getUserIndustry(Context context) {
        initUserSp(context);
        String userIndustry = userSp.getString(UserIndustry, "");
        return userIndustry;
    }

    public static void setUserIndustry(String userIndustry, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserIndustry, userIndustry);
        editor.commit();
    }

    //好友数
    public static int getUserFriendNum(Context context) {
        initUserSp(context);
        int userFriendNum = userSp.getInt(UserFriendNum, 0);
        return userFriendNum;
    }

    public static void setUserFriendNum(int userFriendNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(UserFriendNum, userFriendNum);
        editor.commit();
    }

    //关注的好友数
    public static int getUserFocusNum(Context context) {
        initUserSp(context);
        int userFocusNum = userSp.getInt(UserFocusNum, 0);
        return userFocusNum;
    }

    public static void setUserFocusNum(int userFocusNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(UserFocusNum, userFocusNum);
        editor.commit();
    }

    //发布的帖子数
    public static int getUserPostNum(Context context) {
        initUserSp(context);
        int userPostNum = userSp.getInt(UserPostNum, 0);
        return userPostNum;
    }

    public static void setUserPostNum(int userPostNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(UserPostNum, userPostNum);
        editor.commit();
    }

    //收藏的帖子数
    public static int getUserFocusPostNum(Context context) {
        initUserSp(context);
        int userFocusPostNum = userSp.getInt(UserFocusPostNum, 0);
        return userFocusPostNum;
    }

    public static void setUserFocusPostNum(int userFocusPostNum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putInt(UserFocusPostNum, userFocusPostNum);
        editor.commit();
    }

    //个人关注类别
    public static String getUserFollowIDS(Context context) {
        initUserSp(context);
        String appUserUrl = userSp.getString(UserFollowIDS, "");
        return appUserUrl;
    }


    public static void setUserFollowIDS(String userFollowIDS, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserFollowIDS, userFollowIDS);
        editor.commit();
    }

    //个人关注类别
    public static String getUserFollowUIDS(Context context) {
        initUserSp(context);
        String appUserUrl = userSp.getString(UserFollowUIDS, "");
        return appUserUrl;
    }


    public static void setUserFollowUIDS(String userFollowUIDS, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserFollowUIDS, userFollowUIDS);
        editor.commit();
    }

    //个人头像信息
    public static String getAppUserUrl(Context context) {
        initUserSp(context);
        String appUserUrl = userSp.getString(AppUserUrl, "");
        return appUserUrl;
    }


    public static void setAppUserUrl(String appUserUrl, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(AppUserUrl, appUserUrl);
        editor.commit();
    }

    public static String getAuthToken(Context context) {
        initUserSp(context);
        String token = userSp.getString(KeyAuthToken, "");
        return token;
    }

    public static void setPhone(String userPhone, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(KeyPhone, userPhone);
        editor.commit();
    }

    public static void setUserAddress(String userAddress, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserAddress, userAddress);
        editor.commit();
    }

    public static String getUserAddress(Context context) {
        initUserSp(context);
        String userAddress = userSp.getString(UserAddress, "");
        return userAddress;
    }

    public static String getPhone(Context context) {
        initUserSp(context);
        String userPhone = userSp.getString(KeyPhone, "");
        return userPhone;
    }

    public static void setUserType(String userType, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserType, userType);
        editor.commit();
    }

    public static String getUserType(Context context) {
        initUserSp(context);
        String userType = userSp.getString(UserType, "0");
        return userType;
    }

    public static void setAuthToken(String authToken, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(KeyAuthToken, authToken);
        editor.commit();
    }

    public static boolean getFirstLogin(Context context) {
        initUserSp(context);
        return userSp.getBoolean(KeyFirstLogin, true);
    }

    public static void setLoginState(boolean isLogin, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putBoolean(KeyLoginState, isLogin);
        editor.commit();
    }

    public static boolean getLoginState(Context context) {
        initUserSp(context);
        return userSp.getBoolean(KeyLoginState, false);
    }

    public static String getAppUserId(Context context) {
        initUserSp(context);
        return userSp.getString(AppUserId, "0");
    }

    public static void setAppUserId(String appUserId, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(AppUserId, appUserId);
        editor.commit();
    }

    public static void setUserName(String name, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserName, name);
        editor.commit();
    }

    public static void setNikeName(String name, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(NickName, name);
        editor.commit();
    }

    public static void setUserSum(String userSum, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserSum, userSum);
        editor.commit();
    }

    public static void setPayPassWord(String payPassWord, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(PayPassWord, payPassWord);
        editor.commit();
    }

    public static String getUserName(Context context) {
        initUserSp(context);
        String name = userSp.getString(UserName, "");
        return name;
    }

    public static String getNickName(Context context) {
        initUserSp(context);
        String name = userSp.getString(NickName, "");
        return name;
    }

    public static String getUserSum(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserSum, "0.00");
        return usersum;
    }
    public static String getPayPassWord(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(PayPassWord, "");
        return usersum;
    }

    public static void clear(Context context) {

        setAuthToken("", context);
        setLoginState(false, context);
        setUserName("", context);
        setNikeName("", context);
        setUserSum("", context);
        setPhone("", context);
        setPayPassWord("", context);
        setAppUserId("0", context);
        setAppUserUrl("", context);

    }


    public static String getUserInfoData1(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData1, "");
        return usersum;
    }
    public static String getUserInfoData2(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData2, "");
        return usersum;
    }
    public static String getUserInfoData3(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData3, "");
        return usersum;
    }
    public static String getUserInfoData4(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData4, "");
        return usersum;
    }
    public static String getUserInfoData5(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData5, "");
        return usersum;
    }
    public static String getUserInfoData6(Context context) {
        initUserSp(context);
        String usersum = userSp.getString(UserInfoData6, "");
        return usersum;
    }

    public static void setUserInfoData1(String userInfoData1, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData1);
        editor.commit();
    }
    public static void setUserInfoData2(String userInfoData2, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData2);
        editor.commit();
    }
    public static void setUserInfoData3(String userInfoData3, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData3);
        editor.commit();
    }
    public static void setUserInfoData4(String userInfoData4, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData4);
        editor.commit();
    }
    public static void setUserInfoData5(String userInfoData5, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData5);
        editor.commit();
    }
    public static void setUserInfoData6(String userInfoData6, Context context) {
        initUserSp(context);
        SharedPreferences.Editor editor = userSp.edit();
        editor.putString(UserInfoData1, userInfoData6);
        editor.commit();
    }

}
