package com.caesar.gxmz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;

@SuppressLint("JavascriptInterface")
public class TestWebScript extends Activity {
    private WebView webview;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("http://dev.h5-geshow.wiyoo.cn/#/dynamic/index");
//        webview.loadUrl("file:///android_asset/show.html");
        webview.addJavascriptInterface(this, "app");
//        webview.addJavascriptInterface(new App(), "app");
    }

    @JavascriptInterface
    public void actionFormJs(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Log.d("actionFormJs",msg);
    }

    //注入JavaScript的Java类
    class App {
        @JavascriptInterface
        public void actionFormJs(String title) {
            ToastUtils.showLong(title);
            Log.d("App",title);
        }
    }

}
