package com.caesar.gxmz.network;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 43053 on 2017/1/4.
 */

public class AppNetworkUtils extends NetworkUtils {

    //生成api
    public static Api initRetrofitApi() {
        return initRetrofitApi(new ApiConfig().getServer(), Api.class);
    }

    //生成包含Headers的api
    public static Api initHeadersRetrofitApi() {
        Map<String, String> headersMap = new HashMap<>();
        try {
            headersMap.put("Version", "h5_v1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return initHeadersRetrofitApi(new ApiConfig().getServer(), Api.class, headersMap);
    }

    //生成公共api
    public static Api initPublicRetrofitApi() {
        return initRetrofitApi(new ApiConfig().getServer(), Api.class);
    }


}
