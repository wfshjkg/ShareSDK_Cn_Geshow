package com.caesar.gxmz.network;

/**
 * Created by mathum on 2019/3/26.
 */

public class ApiConfig {

    private static final String TESTSERVER = "http://app.geshow.com/";
    public static final String FORMALSERVER = "http://h5.geshow.com/";
    public static int CODE_SUCC = 101;
    public static String THINKCMF_PATH = "http://qiniu.500-china.com/";
    public static String IMAGE_PATH = "https://geshow-shop.oss-cn-hangzhou.aliyuncs.com";
    public String getServer() {
        return FORMALSERVER;
    }

}
