package com.caesar.gxmz.network;

import android.util.Log;

/**
 * Log工具类
 * Created by 43053 on 2016/6/16.
 */
public class L {

    public static void l(String tag,String message){
        if (Config.isDebug){
            Log.i(tag,message);
        }
    }

}
