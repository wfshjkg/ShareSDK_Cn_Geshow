package com.caesar.gxmz.network;

import com.caesar.gxmz.data.AppAdvertVideoBean;
import com.caesar.gxmz.data.DynamicData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by mathum on 2017/1/4.
 */

public interface Api {

    /*获取用户订单列表*/
    @GET("index.php?g=user&m=index&a=getAppSlideList")
    Call<AppAdvertVideoBean> getAppSlideList();

    /*获取用户订单列表*/
    @FormUrlEncoded
    @POST("api/dynamic/getDynamicDetail")
    Call<DynamicData> getDynamicDetail(@Field("dynamic_id") String dynamic_id);
}
