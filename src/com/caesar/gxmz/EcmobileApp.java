package com.caesar.gxmz;
//
//                       __
//                      /\ \   _
//    ____    ____   ___\ \ \_/ \           _____    ___     ___
//   / _  \  / __ \ / __ \ \    <     __   /\__  \  / __ \  / __ \
//  /\ \_\ \/\  __//\  __/\ \ \\ \   /\_\  \/_/  / /\ \_\ \/\ \_\ \
//  \ \____ \ \____\ \____\\ \_\\_\  \/_/   /\____\\ \____/\ \____/
//   \/____\ \/____/\/____/ \/_//_/         \/____/ \/___/  \/___/
//     /\____/
//     \/___/
//
//  Powered by BeeFramework
//

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;

import com.blankj.utilcode.util.Utils;

import java.io.File;

public class EcmobileApp extends Application
{

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
    }

    public void startAppActivity(Intent sendIntent) {
        startActivity(sendIntent);
    }
}